import numpy
import sys
import cv2
import morpho
import strel

def traiterImage(image):

    #On vire un canal de couleur
    image1SeulCanal = image[:,:,0]

    #On seuille l'image
    imageSeuilee = numpy.zeros(image1SeulCanal.shape, image1SeulCanal.dtype)
    imageSeuilee[image1SeulCanal > 85] = 255

    imageSeuilee = 255 - imageSeuilee

    #On crée une image blanche
    imageAvecBordure = numpy.ones(imageSeuilee.shape, imageSeuilee.dtype) * 255

    #On vire les pixels sur la bordure
    imageAvecBordure[1:-2, 1:-2] = 0

    #On ne garde la bordure que là où il y a un pixel blanc sur l'image seuillée
    imageAvecBordure = numpy.minimum(imageSeuilee, imageAvecBordure)

    #Va remettre en blanc tout ce qui touche à la bordure sur l'image seuillée
    rec = morpho.reconstruction_inferieure(imageSeuilee, imageAvecBordure, strel.build('carre', 1))

    #On vire la partie blanche de l'image seuillée
    imageTraitee = imageSeuilee - rec

    #imageTraiteeOuverte = morpho.ouverture(imageTraitee, strel.build('carre', 2))
    imageTraiteeOuverte = morpho.ouvertureconstruction(imageTraitee, strel.build('disque', 5), strel.build('carre', 1))

    imageGradient = morpho.gradient(imageTraiteeOuverte, strel.build('diamant', 2))

    image[imageGradient == 255] = [0,0,255]

    #On retourne l'image et les coordonnées de la bande blanche
    return image, imageTraiteeOuverte == 255


image = cv2.imread(sys.argv[1])
imageNB = cv2.imread(sys.argv[2])

imageRes, coordsBande = traiterImage(image)

cv2.imshow('Resultat', imageRes)
cv2.waitKey(0)

#On regarde si la moyenne des pixels de la bande blanche est quand même vachement blanche
if numpy.mean(imageNB[coordsBande]) > 200:
    print("Isok 👌")
else:
    print("Glouglou c'est la noyade 🐳")