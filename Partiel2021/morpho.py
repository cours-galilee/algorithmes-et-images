import cv2
import numpy as np
import math
import strel

# Fonction permettant de realiser l'erosion d'une image par un element structurant
def erosion(image, element_structurant):
    return cv2.erode(image, element_structurant)

# Fonction permettant de realiser la dilatation d'une image par un element structurant
def dilatation(image, element_structurant):
    return cv2.dilate(image, element_structurant)

# Fonction permettant de realiser l'ouverture d'une image par un element structurant
def ouverture(image, element_structurant):
    return dilatation(erosion(image, element_structurant), element_structurant)

# Fonction permettant de realiser la fermeture d'une image par un element structurant
def fermeture(image, element_structurant):
    return erosion(dilatation(image, element_structurant), element_structurant)

# Fonction permettant de calculer le gradient morphologique d'une image (par rapport a un element structurant)
def gradient(image, element_structurant):
    return dilatation(image, element_structurant) - erosion(image, element_structurant)

# Fonction permettant de realiser la reconstructions inferieure d'un marqueur dans une image, a l'aide d'un element structurant
def reconstruction_inferieure(image, marqueur, element_structurant):
    backup = marqueur
    #On fait une dilatation conditionnelle du marqueur dans l'image
    #A completer
    dil = np.minimum(dilatation(marqueur, element_structurant), image)
    #Tant que la dilatation conditionnelle n'a pas converge
    while( not np.array_equal(backup, dil)):
        backup = dil
        #On refait une dilatation conditionnelle de dil dans image
        #A completer
        dil = np.minimum(dilatation(dil, element_structurant), image)
    return dil



# Fonction permettant de realiser l'ouverture par reconstruction d'une image
def ouverture_reconstruction(image, element_ouverture, element_reconstruction):
    return reconstruction_inferieure(image, ouverture(image, element_ouverture), element_reconstruction)



#Fonction permettant de realiser le h-maxima d'une image
#Prend en parametre l'image, le niveau h, et l'element structurant a utiliser pour la reconstruction
def hmaxima(image, h, element_reconstruction):
    #On rehausse toutes les valeurs de image inferieure a h, a la valeur h
    image = np.maximum(np.ones(image.shape, image.dtype)*h, image)
    #On soustrait h a image
    image2 = image - h
    #A completer
    return reconstruction_inferieure(image, image2, element_reconstruction)

#Fonction permettant de realiser le seuil d'une image
#Prend en parametre l'image ainsi que la valeur du seuil
def seuil(image, seuil):
    imageSup = np.zeros(image.shape, image.dtype)
    imageSup[image > seuil] = 255
    return imageSup

def ouvertureconstruction(image, elemStructurantOuverture, elemStructurantReconst):
    imageOuverte = ouverture(image, elemStructurantOuverture)
    return reconstruction_inferieure(image, imageOuverte, elemStructurantReconst)