import cv2


def myerode(image, elm):
    return cv2.erode(image, elm)

def mydilate(image, elm):
    return cv2.dilate(image, elm)

def mygrad(image, elm) :
    return mydilate(image, elm) - myerode(image, elm)

image = cv2.imread('../../fichiers_TP1/Images/chien.png')
# erosion = myerode(image, strel.build('ligne', 15, 90))
#
# cv2.imshow('Image', erosion)
# cv2.waitKey(0)

# dilatation = mydilate(image, strel.build('disque', 3))
#
# cv2.imshow('Image', dilatation)
# cv2.waitKey(0)

#cv2.imshow('Image', mygrad(image, strel.build('disque', 1)))
#cv2.waitKey(0)
