import sys
import cv2
from Commun.strel import build
from Commun.morpho import *

# image = cv2.imread('../fichiers_TP1/Images/papier_60.png')
image = cv2.imread(sys.argv[1])
# On ne garde que le canal rouge
feuille = image[:, :, 2]

results = []
for angle in range(-90, 91, 1):
    feuilleGrad = mygrad(feuille, build('ligne', 40, angle))
    nbPixels = feuilleGrad.sum()
    results += [(nbPixels, angle)]

print(min(results)[1])
