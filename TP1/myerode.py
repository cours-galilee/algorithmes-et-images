import cv2
import numpy
import sys


def my_first_erode(image):
    new_image = numpy.zeros(image.shape, image.dtype)
    [h, l] = image.shape[0:2]
    for i in range(1, h - 1):
        for j in range(1, l - 1):
            new_image[i, j] = numpy.amin(image[i-1:i+2, j-1:j+2])
    return new_image


def my_first_dilate(image):
    return 255 - my_first_erode(255 - image)


if len(sys.argv) < 3:
    print("Usage : myerode.py <fichier_entree> <fichier_sortie>", file=sys.stderr)
    exit(1)

img = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)
cv2.imwrite(sys.argv[2], my_first_erode(img))
