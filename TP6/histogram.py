import cv2
import numpy
from Commun.display import display_img
from Commun.myimage import affiche_histogram

def getHistoCumule(histo):
    histoC = histo.copy()
    for i in range(1, len(histo)):
        histoC[i] = histoC[i-1] + histoC[i]
    return histoC

def egalhisto(P):
    histoCumule = getHistoCumule(cv2.calcHist([P], [0], None, [256], [0,256]))
    N = P.shape[0] * P.shape[1]

    Q = numpy.zeros(P.shape, P.dtype)
    for niveau in range(0,256):
        Q[P == niveau] = 255/N * histoCumule[niveau]

    return Q

def egalHistoCoulbyHSV(P):
    pHSV = cv2.cvtColor(P, cv2.COLOR_BGR2HSV)

    qHSV = pHSV.copy()
    qHSV[:, :, 2] = egalhisto(qHSV[:, :, 2])
    return cv2.cvtColor(qHSV, cv2.COLOR_HSV2BGR)

img = cv2.imread('Fichiers/images/chambre.png', cv2.IMREAD_GRAYSCALE)
q = egalhisto(img)
img2 = cv2.imread('Fichiers/images/PJ.jpg', cv2.IMREAD_GRAYSCALE)
q2 = egalhisto(img2)

display_img('Entree', img, 1)
display_img('Sortie', q)

hp = cv2.calcHist([img], [0], None, [256], [0, 256])
hq = cv2.calcHist([q], [0], None, [256], [0, 256])

affiche_histogram([hp, hq], [[0,0,0], [255,0,0]], 600, 500)


display_img('Entree', img2, 1)
display_img('Sortie', q2)

hp2 = cv2.calcHist([img2], [0], None, [256], [0, 256])
hq2 = cv2.calcHist([q2], [0], None, [256], [0, 256])

affiche_histogram([hp2, hq2], [[0,0,0], [255,0,0]], 600, 500)


imgChat = cv2.imread('Fichiers/images/chat.jpg');
chatEgalise = numpy.zeros(imgChat.shape, imgChat.dtype)
for i in range(0,3):
    chatEgalise[:,:,i] = egalhisto(imgChat[:,:,i])

display_img('Entree', imgChat, 1)
display_img('Sortie', chatEgalise)

hpChatB = cv2.calcHist([imgChat[:,:,0]], [0], None, [256], [0, 256])
hpChatV = cv2.calcHist([imgChat[:,:,1]], [0], None, [256], [0, 256])
hpChatR = cv2.calcHist([imgChat[:,:,2]], [0], None, [256], [0, 256])
hqChatB = cv2.calcHist([chatEgalise[:,:,0]], [0], None, [256], [0, 256])
hqChatV = cv2.calcHist([chatEgalise[:,:,1]], [0], None, [256], [0, 256])
hqChatR = cv2.calcHist([chatEgalise[:,:,2]], [0], None, [256], [0, 256])

affiche_histogram([hpChatB, hpChatV, hpChatR], [[255,0,0], [0,255,0], [0,0,255]], 600, 500)
affiche_histogram([hqChatB, hqChatV, hqChatR], [[255,0,0], [0,255,0], [0,0,255]], 600, 500)
# Le chat est plus bleu, car il y avait moins de bleu dans l'image de départ


imgChatEgalisee = egalHistoCoulbyHSV(imgChat)

display_img('Sortie', imgChatEgalisee, 1)
hq2Chat = [
    cv2.calcHist([imgChatEgalisee[:,:,0]], [0], None, [256], [0, 256]),
    cv2.calcHist([imgChatEgalisee[:,:,1]], [0], None, [256], [0, 256]),
    cv2.calcHist([imgChatEgalisee[:,:,2]], [0], None, [256], [0, 256])
]
affiche_histogram(hq2Chat, [[255,0,0], [0,255,0], [0,0,255]], 600, 500)


# le chat est désormais de la bonne couleur
