import cv2
from Commun.myimage import affiche_histogram

img = cv2.imread('Fichiers/images/chambre.png', cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread('Fichiers/images/chat.jpg')

histo = cv2.calcHist([img[:, :]], [0], None, [256], [0, 256])
print(histo)
print(len(histo))

hb = cv2.calcHist([img2[:, :, 0]], [0], None, [256], [0, 256])
hv = cv2.calcHist([img2[:, :, 1]], [0], None, [256], [0, 256])
hr = cv2.calcHist([img2[:, :, 2]], [0], None, [256], [0, 256])

affiche_histogram([histo], [[0,0,0]], 600, 500)
affiche_histogram([hb, hv, hr], [[255,0,0], [0,255,0], [0,0,255]], 600, 500)