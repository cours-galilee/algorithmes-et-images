import cv2

def display_img(name, file, delay=0):
    cv2.imshow(name, file)
    cv2.waitKey(delay)