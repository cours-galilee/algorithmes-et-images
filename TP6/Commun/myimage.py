import numpy as np
import cv2

def affiche_histogram(hist, color, largeur, hauteur):
    #On definit les abscisses des points que l'on souhaite tracer
    bins = np.array(range(0,256))*largeur/256
    #L'image de sortie, qu'on initialise tout en blanc
    imdisplay = np.ones((hauteur, largeur, 3))*255
    c=0 #Compteur pour les couleurs
    for i in hist:
        nhistogram = np.copy(i)
        #On normalise les valeurs de l'histogramme entre 0 et hauteur-1
        cv2.normalize(i, nhistogram, 0, hauteur-1, cv2.NORM_MINMAX)
        inthisto = np.int32(np.around(nhistogram)) #Conversion en entier
        #On cree un nouveau tableau rassemblant bins (les abscisses) et inhisto (les points)
        pts = np.column_stack((bins,inthisto))
        #Et on trace la courbe
        cv2.polylines(imdisplay, np.int32([pts]), False, color[c])
        c+=1

    imdisplay = np.flipud(imdisplay) #Retournement vertical
    cv2.imshow('Histogramme', imdisplay)
    cv2.waitKey(0)
