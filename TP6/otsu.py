import numpy
import cv2

from Commun.display import display_img
from Commun.myutil import myseuil

def getHistoCumule(histo):
    histoC = histo.copy()
    for i in range(1, len(histo)):
        histoC[i] = histoC[i-1] + histoC[i]
    return histoC

def otsu2(img):
    H = cv2.calcHist([img], [0], None, [256], [0,256])
    N = img.shape[0] * img.shape[1]
    N2 = N*N

    mu = numpy.mean(img)

    g1 = H[0]
    g2 = N-g1
    mu1 = 0
    mu2 = mu*N / g2

    best_vInter = (g1 * g2 / N2) * (mu1-mu2)**2
    bestSeuil = 0

    for s in range(1,256):
        mu1 = mu1*g1 + s*H[s]
        mu2 = mu2*g2 - s*H[s]

        g1 = g1 + H[s]
        g2 = g2 - H[s]
        mu1 = mu1/g1
        mu2 = mu2/g2

        print(mu1, numpy.mean(img[img <= s]))
        print(mu2, numpy.mean(img[img > s]))

        vInter = (g1 * g2 / N2) * (mu1-mu2)**2
        if vInter > best_vInter:
            best_vInter = vInter
            bestSeuil = s

    return bestSeuil

def otsu4(img):
    H = cv2.calcHist([img], [0], None, [256], [0,256])
    N = img.shape[0] * img.shape[1]
    N2 = N*N

    mu = numpy.mean(img)

    g1 = H[0]
    g2 = H[1]
    g3 = H[1]
    g4 = N-g1-g2-g3
    mu1 = 0
    mu2 = 1
    mu3 = 2
    mu4 = numpy.mean(img>=3)

    best_vInter = (g1 * g2 / N2) * (mu1-mu2)**2
    bestSeuil = 0

    for s in range(0,256):
        mu1 = mu1*g1 + s*H[s]
        mu2 = mu2*g2 - s*H[s]

        g1 = g1 + H[s]
        g2 = g2 - H[s]
        mu1 = mu1/g1
        mu2 = mu2/g2

        vInter = (g1 * g2 / N2) * (mu1-mu2)**2
        if vInter > best_vInter:
            best_vInter = vInter
            bestSeuil = s

    return bestSeuil

imgVille = cv2.imread('Fichiers/images/ville.png', cv2.IMREAD_GRAYSCALE)
display_img('Ville', imgVille, 1)

seuilVille = otsu2(imgVille)
imgVilleSeuil = myseuil(imgVille, seuilVille)

display_img('Ville Seuil', imgVilleSeuil)


imgSpider = cv2.imread('Fichiers/images/spider.jpg', cv2.IMREAD_GRAYSCALE)
display_img('Spider', imgSpider, 1)

imgSpiderSeuil = myseuil(imgSpider, otsu2(imgSpider))

display_img('Spider Seuil', imgSpiderSeuil)