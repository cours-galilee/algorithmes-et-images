import cv2

from Commun.strel import build
from Commun.myutil import *
from Commun.morpho import *

image = cv2.imread('Fichiers_TP2/Images/SaltPepper.png')
elmOuv = build('ligne', 2, 90)
elmFerm = build('disque', 2)

better = myclose(myopen(image, elmOuv), elmFerm)

cv2.imshow('Image', better)
cv2.waitKey(0)
