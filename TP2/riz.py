import sys

import cv2
import numpy

from Commun.morpho import *
from Commun.myutil import *
from Commun.strel import build

[imagePath, r, s] = sys.argv[1:4]
r = float(r)
s = int(s)

# [imagePath, r, s] = ["Fichiers_TP2/Images/rice.png", 10, 40]

image = cv2.imread(imagePath, cv2.IMREAD_GRAYSCALE)
if image is None:
    print("Image non trouvée")
    exit(0)

elm = build('disque', r)
fond = myopen(image, elm)
tophat = image - fond

# s = myseuil_interactif(tophat)
tophat_seuil = myseuil(tophat, s)

cv2.imwrite('rice.png', tophat_seuil)

# on ajoute une bordure noire à l'image
imgBorder = numpy.zeros((tophat_seuil.shape[0] + 2, tophat_seuil.shape[1] + 2), numpy.uint8)
imgBorder[1:-1, 1:-1] = tophat_seuil

i = 1
step = 1.0
for j in range(0, 5):
    while myopen(imgBorder, build('disque', i)).sum() > 0:
        i += step
    i -= step
    step /= 10

print(str(i*2 + 1))

# cv2.imshow('Image', imgBorder)
# cv2.waitKey(0)
# cv2.imshow('Image', myopen(imgBorder, build('disque', i)))
# cv2.waitKey(0)
