import sys

import cv2
import numpy

from Commun.morpho import *
from Commun.myutil import *
from Commun.strel import build

[imageInPath, l, s, imageOutPath] = sys.argv[1:5]
l = int(l)
s = int(s)

image = cv2.imread(imageInPath)

cv2.imshow('image', image)
cv2.waitKey(0)

image = numpy.amax(image, 2) - numpy.amin(image, 2)
image = myopen(image, build('disque', 2))

# on ajoute une bordure noire à l'image
image[0,:]  = 255
image[-1,:] = 255
image[:,0]  = 255
image[:,-1] = 255

cv2.imshow('iamge', image)
cv2.waitKey(0)


filteredImage = 255-numpy.zeros(image.shape, numpy.uint8)

for i in range(0, 180):
    img = myclose(image, build('ligne', l, i))
    filteredImage = numpy.minimum(filteredImage, img)

filteredImage = 255-filteredImage

s = myseuil_interactif(filteredImage)
imageSeuil = myseuil(filteredImage, s)

imageSeuil = myclose(imageSeuil, build('diamant', 1))
imageSeuil = myopen(imageSeuil, build('carre', 2))

gradient = mygrad(imageSeuil, build('diamant', 1))
cv2.imshow('i',gradient)
cv2.waitKey(0)

image = cv2.imread(imageInPath)
image[gradient == 255] = [0,0,255]

cv2.imwrite(imageOutPath, image)
