import sys

import cv2
import numpy

from Commun.morpho import *
from Commun.myutil import *
from Commun.strel import build

[imageInPath, l, s, imageOutPath] = sys.argv[1:5]
l = int(l)
s = int(s)

image = cv2.imread(imageInPath, cv2.IMREAD_GRAYSCALE)

filteredImage = 255-numpy.zeros(image.shape, numpy.uint8)

for i in range(0, 180):
    img = myclose(image, build('ligne', l, i))
    filteredImage = numpy.minimum(filteredImage, img)

filteredImage = 255-filteredImage

# s = myseuil_interactif(filteredImage)
imageSeuil = myseuil(filteredImage, s)

imageSeuil = myclose(imageSeuil, build('diamant', 1))
imageSeuil = myopen(imageSeuil, build('carre', 2))

cv2.imwrite(imageOutPath, imageSeuil)