import cv2

from Commun.strel import build
from Commun.myutil import *
from Commun.morpho import *

image = cv2.imread('Fichiers_TP2/Images/numbers.png')
elm = build('carre', 1)

fond = myopen(image, elm)
tophat = image - fond

s = myseuil_interactif(tophat)
tophat_seuil = myseuil(tophat, s)

cv2.imshow('Image', tophat_seuil)
cv2.waitKey(0)