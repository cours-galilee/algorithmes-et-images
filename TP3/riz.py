import random
import sys

import cv2
import numpy

from Commun.display import *
from Commun.morpho import *
from Commun.myutil import *
from Commun.strel import build
import Commun.display


def getLargeur(im):
    largeur = 1.0
    step = 0.1
    while myopen(im, build('disque', largeur)).sum() > 0:
        largeur += step
    return (largeur-step)*2 + 1

def getLongueur(im):
    longueur = 1.0
    step = 0.1
    for angle in range(0, 180):
        while myopen(im, build('ligne', longueur, angle)).sum() > 0:
            longueur += step
    return (longueur-step) * 2 +1

def makeGrayscaleMap(im, minCol=50, maxCol=255, fond=0):
    maxVal = numpy.amax(im[im != fond])
    minVal = numpy.amin(im[im != fond])
    detlaVal = maxVal - minVal

    deltaColor = maxCol - minCol

    colorMap = numpy.zeros(im.shape, im.dtype)
    colorMap[im != fond] = ((im[im != fond] - minVal) / detlaVal * deltaColor + minCol)
    return numpy.uint8(colorMap)

def makeGrayscaleMapMinMax(im, minCol, maxCol, minVal, maxVal, fond=0):
    detlaVal = maxVal - minVal

    deltaColor = maxCol - minCol

    cp = im.copy()
    im[im > maxVal] = maxVal
    im[im < minVal] = minVal
    im[cp == fond] = fond

    colorMap = numpy.zeros(im.shape, im.dtype)
    colorMap[im != fond] = ((im[im != fond] - minVal) / detlaVal * deltaColor + minCol)
    return numpy.uint8(colorMap)


def getAireRiz(im):
    return numpy.sum(im > 0)


def getR(im):
    longMesure = getLongueur(im)
    longEstimee = 2 * getAireRiz(im) / (getLargeur(im)/2 * numpy.pi)
    return (1 - longMesure/longEstimee)**2

def filterRice(im, seuil, rayonOpen):
    elm = build('disque', rayonOpen)
    fond = myopen(im, elm)
    tophat = im - fond

    # s = myseuil_interactif(tophat)
    return myseuil(tophat, seuil)



s = 60
r = 12
[inp, out1, out2, out3] = sys.argv[1:5]

image = cv2.imread(inp, cv2.IMREAD_GRAYSCALE)
image = filterRice(image, s, r)
image = myouvrecon(image, build('carre', 2), build('diamant', 1))

# Suppression des grain de riz au bord :
masqueRemplissage = numpy.zeros(image.shape, image.dtype)
masqueRemplissage[1:-1, 1:-1] = 255
masqueRemplissage = numpy.minimum(255 - masqueRemplissage, image)

imageNoBord = image - myreconinf(image, build('diamant', 1), masqueRemplissage)

colorsLarg = numpy.zeros(imageNoBord.shape)
colorsLong = numpy.zeros(imageNoBord.shape)
colorsR = 255-numpy.zeros(imageNoBord.shape)
restants = imageNoBord

while numpy.sum(restants) > 0:
    m = numpy.zeros(imageNoBord.shape, imageNoBord.dtype)

    indexes = numpy.where(restants == 255)
    riceCoord = (indexes[0][0], indexes[1][0])
    m[riceCoord] = 255

    reconstruction = myreconinf(imageNoBord, build('carre', 1), m)
    restants = restants - reconstruction

    colorsLarg[reconstruction == 255] = getLargeur(reconstruction)
    colorsLong[reconstruction == 255] = getLongueur(reconstruction)
    colorsR[reconstruction == 255] = R = getR(reconstruction)


# nbRiz = numpy.sum(mAll > 0)

colorMapLarg = makeGrayscaleMap(colorsLarg)
# display_img('Color Map Largeur NB', colorMapLarg)
# colorMapLarg = cv2.applyColorMap(colorMapLarg, cv2.COLORMAP_JET)
# colorMapLarg[colorsLarg == 0] = 0
# display_img('Color Map Largeur', colorMapLarg)

colorMapLong = makeGrayscaleMap(colorsLong, 30, 255)
# display_img('Color Map Longueur NB', colorMapLong)
# colorMapLong = cv2.applyColorMap(colorMapLong, cv2.COLORMAP_JET)
# colorMapLong[colorsLong == 0] = 0
# display_img('Color Map Longueur', colorMapLong)

colorMapR = makeGrayscaleMapMinMax(colorsR, 30, 255, 0, 0.15, 255)
# display_img('Color Map R NB', colorMapR)

# print(numpy.amax(colorsR[colorsR!=255]))

cv2.imwrite(out1, colorMapLarg)
cv2.imwrite(out2, colorMapLong)
cv2.imwrite(out3, colorMapR)



