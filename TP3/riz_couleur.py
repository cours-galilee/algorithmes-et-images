import random
import sys

import cv2
import numpy

from Commun.display import *
from Commun.morpho import *
from Commun.myutil import *
from Commun.strel import build


image = cv2.imread('rice.png', cv2.IMREAD_GRAYSCALE)
image = myouvrecon(image, build('carre', 2), build('diamant', 1))

# Suppression des grain de riz au bord :
masqueRemplissage = numpy.zeros(image.shape, image.dtype)
masqueRemplissage[1:-1, 1:-1] = 255
masqueRemplissage = numpy.minimum(255 - masqueRemplissage, image)

imageNoBord = image - myreconinf(image, build('diamant', 1), masqueRemplissage)

# coloration des grains de riz
mAll = numpy.zeros(imageNoBord.shape, imageNoBord.dtype)
colors = numpy.zeros(imageNoBord.shape + (3,), imageNoBord.dtype)
restants = imageNoBord

while numpy.sum(restants) > 0:
    m = numpy.zeros(imageNoBord.shape, imageNoBord.dtype)

    indexes = numpy.where(restants == 255)
    riceCoord = (indexes[0][0], indexes[1][0])
    mAll[riceCoord] = m[riceCoord] = 255

    reconstruction = myreconinf(imageNoBord, build('carre', 1), m)
    restants = restants - reconstruction

    colors[reconstruction == 255] = [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)]

display_img('Col', colors)

nbRiz = numpy.sum(mAll > 0)
print(nbRiz)