import cv2

def display_img(name, img, delay=0):
    cv2.imshow(name, img)
    cv2.waitKey(delay)