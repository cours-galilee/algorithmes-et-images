import cv2
import numpy


def myerode(image, elm):
    return cv2.erode(image, elm)


def mydilate(image, elm):
    return cv2.dilate(image, elm)


def mygrad(image, elm):
    return mydilate(image, elm) - myerode(image, elm)


def myopen(image, elm):
    return mydilate(myerode(image, elm), elm)


def myclose(image, elm):
    return myerode(mydilate(image, elm), elm)


def myconddilate(image, elm, masque):
    return numpy.minimum(mydilate(masque, elm), image)


def myconderode(image, elm, masque):
    return numpy.maximum(myerode(masque, elm), image)


def myreconinf(image, elm, masque):
    tmp = myconddilate(image, elm, masque)
    while not(numpy.array_equal(masque, tmp)):
        masque = tmp
        tmp = myconddilate(image, elm, masque)
    return tmp


def myreconsup(image, elm, masque):
    tmp = myconderode(image, elm, masque)
    while not(numpy.array_equal(masque, tmp)):
        masque = tmp
        tmp = myconderode(image, elm, masque)
    return tmp


def myouvrecon(image, elm, f):
    return myreconinf(image, f, myopen(image, elm))


def myfermrecon(image, elm, f):
    return myreconsup(image, f, myclose(image, elm))


# erosion = myerode(image, strel.build('ligne', 15, 90))
#
# cv2.imshow('Image', erosion)
# cv2.waitKey(0)

# dilatation = mydilate(image, strel.build('disque', 3))
#
# cv2.imshow('Image', dilatation)
# cv2.waitKey(0)

# cv2.imshow('Image', mygrad(image, strel.build('disque', 1)))
# cv2.waitKey(0)
